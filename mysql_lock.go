// Reuse existing mysql server setup for distributed locking
// https://dev.mysql.com/doc/refman/5.7/en/miscellaneous-functions.html#function_get-lock
// https://www.xaprb.com/blog/2006/07/26/how-to-coordinate-distributed-work-with-mysqls-get_lock/

package main

import (
	"database/sql"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"os/exec"

	_ "github.com/go-sql-driver/mysql"
	"gopkg.in/yaml.v2"
)

type DbConfig struct {
	Host     string `yaml:"host"`
	Port     int    `yaml:"port"`
	User     string `yaml:"user"`
	Password string `yaml:"password"`
}

type Config struct {
	Db DbConfig `yaml:"db"`
}

var (
	cfg Config

	lockAquired bool

	cfgFile     = flag.String("c", "/etc/mysql_lock.yaml", "configuration file")
	lockName    = flag.String("n", "cron", "lock name")
	lockTimeout = flag.Duration("t", 0, "lock acquisition timeout")
)

func main() {
	flag.Usage = func() {
		fmt.Printf("  %s [flags] -- <command> [argument ..]\n\n", os.Args[0])
		flag.PrintDefaults()
		os.Exit(1)
	}

	flag.Parse()

	if flag.NArg() < 1 {
		os.Exit(1)
	}

	cfg = loadConfig(*cfgFile)

	conn, err := sql.Open("mysql", fmt.Sprintf("%s:%s@tcp(%s:%d)/", cfg.Db.User, cfg.Db.Password, cfg.Db.Host, cfg.Db.Port))
	hasError(err)
	defer conn.Close()

	stmtOut, err := conn.Prepare("SELECT COALESCE(GET_LOCK(?, ?), 0)")
	hasError(err)

	err = stmtOut.QueryRow(*lockName, lockTimeout.Seconds()).Scan(&lockAquired)
	hasError(err)

	cmd, args := flag.Args()[0], flag.Args()[1:]

	if lockAquired {
		cmd := exec.Command(cmd, args...)

		cmd.Stdin = os.Stdin
		cmd.Stdout = os.Stdout
		cmd.Stderr = os.Stderr

		err = cmd.Run()
		if err != nil {
			// todo: exit with cmd exit code
			os.Exit(-5)
		}
	} else {
		os.Exit(-10)
	}
}

func hasError(e error) {
	if e != nil {
		log.Fatalln(e)
	}
}

func loadConfig(file string) Config {
	var config Config

	configFile, err := ioutil.ReadFile(file)
	hasError(err)

	err = yaml.Unmarshal(configFile, &config)
	hasError(err)

	return config
}